import React from 'react';
import { Tabs } from './src/config/router.js';

class App extends React.Component {
  render() {
    return(
      <Tabs />
    );
  }
}

export default App;