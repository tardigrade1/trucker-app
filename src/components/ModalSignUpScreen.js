import React, { Component } from 'react';
import { 
	View, 
	Text,
	ScrollView,
	StyleSheet,
	TouchableHighlight
} from 'react-native';

import Tabs from '../config/router.js';

import t from 'tcomb-form-native';
var _ = require('lodash');

const Form = t.form.Form;

var Password = t.refinement(t.String, function (n) { return n.length >= 8; })
Password.getValidationErrorMessage = function (value, path, context) {
	return 'Password should be 8 or more digits'
}

var ConfirmedPassword = t.refinement(t.String, (s) => {
	return s == this.state.User.createPassword;
});
ConfirmedPassword.getValidationErrorMessage = function (value, path, context) {
	return 'Passwords do not match'
}

/*************** form Stylesheet customization *******************
*****************************************************************/

const formStylesheet = _.cloneDeep(t.form.Form.stylesheet);

formStylesheet.textbox.normal.borderWidth = 0;
formStylesheet.textbox.error.borderWidth = 0;
formStylesheet.textbox.normal.marginBottom = 0;
formStylesheet.textbox.error.marginBottom = 0;

formStylesheet.textboxView.normal.borderColor = '#ffffff';
formStylesheet.textboxView.normal.borderWidth = 0;
formStylesheet.textboxView.error.borderWidth = 0;
formStylesheet.textboxView.normal.borderRadius = 0;
formStylesheet.textboxView.error.borderRadius = 0;
formStylesheet.textboxView.normal.borderBottomWidth = 1;
formStylesheet.textboxView.error.borderBottomWidth = 1;
formStylesheet.textbox.normal.marginBottom = 5;
formStylesheet.textbox.error.marginBottom = 5;

/******************** form-stylesheet-end ***********************
*****************************************************************/

const User = t.struct({
	name: t.String,
	email: t.String,
	mobile: t.Number,
	address: t.String,
	licenseNumber: t.Number,
	licenseExpiryDate: t.String,
	commencementDate: t.String,
	createPassword: Password,
	confirmPassword: ConfirmedPassword
})

var options = {
	stylesheet: formStylesheet,
	fields: {
		name: {
			placeholder: 'John Doe',
			help: 'Please enter your full name',
		},
		email: {
			placeholder: 'johndoe@abc.com',
		},
		createPassword: {
			help: 'Must be 8 characters or more',
			password: {
				password: true,
				secureTextEntry: true,
				}
		},
		confirmPassword: {
			password: {
				password: true,
				secureTextEntry: true,
			}
		},
		licenseExpiryDate: {
			placeholder: 'dd/mm/yyyy'
		},
		commencementDate: {
			placeholder: 'dd/mm/yyyy'
		}
	}
}

export default class ModalSignUpScreen extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {};
	}

	onSubmit = (e) => {
		e.preventDefault();
		var value = this.refs.form.getValue();
		const { navigate } = this.props.navigation;
		if (value) {
			console.log(value);
			navigate('Main');
		}
	}

	onChange(value) {
		this.setState({ value });
		if (value.confirmPassword != null && value.confirmPassword !="") {

		}
	}

	clearForm() {
		this.setState({ value: null });
	}

	render() {
		return(
			<ScrollView>
				<View style={styles.container}>
					<Form
						ref="form"
						type={User}
						options={options}
						onChange={(value) => this.onChange(value)}
					/>

					<TouchableHighlight style={styles.button} onPress={this.onSubmit.bind(this)}>
						<Text style={styles.buttonText}>Sign Up</Text>
					</TouchableHighlight>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    margin: 0,
    padding: 20,
    backgroundColor: '#FF7F00'
  },
  buttonText: {
    fontSize: 18,
    color: '#FF7F00',
    alignSelf: 'center'
  },
  button: {
    height: 48,
    width: 100,
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  }
});