import React, { Component } from 'react';

import {
	View,
	Text,
	TouchableHighlight,
	StyleSheet
} from 'react-native';

export default class WelcomeScreen extends Component {
	render() {
		return(
			<View style={styles.container}>
				<Text style={styles.welcomeText}>Hello User, you are at the magical welcome screen</Text>
				<TouchableHighlight style={styles.button} onPress={() => this.props.navigation.navigate('SignUpModal')}>
					<Text style={styles.buttonText}>Sign Up</Text>
				</TouchableHighlight>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    margin: 0,
    padding: 20,
    backgroundColor: '#FF7F00',
    flex: 6
  },
  welcomeText: {
  	justifyContent: 'center',
  	fontSize: 18,
  	color: '#ffffff'
  },
  buttonText: {
    fontSize: 18,
    color: '#FFFFFF',
    alignSelf: 'center'
  },
  button: {
    height: 48,
    width: 100,
    borderColor: '#ffffff',
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  }
});