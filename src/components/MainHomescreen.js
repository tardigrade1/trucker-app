import React from 'react';
import {
	View,
	Text
} from 'react-native';

export default class MainHomescreen extends React.Component {

	render() {
		return(
			<View>
				<Text>This is the main homescreen.</Text>
			</View>
		);
	}
}