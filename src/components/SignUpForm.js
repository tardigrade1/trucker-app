import React, { Component } from 'react';
import { 
	View, 
	Text,
	ScrollView,
	StyleSheet,
	TouchableHighlight
} from 'react-native';

import t from 'tcomb-form-native';

const Form = t.form.Form;

var Password = t.refinement(t.String, function (n) { return n.length >= 8; })
Password.getValidationErrorMessage = function (value, path, context) {
	return 'Password should be 8 or more digits'
}

var confirmedPassword = t.refinement(t.String, (s) => {
	return s == this.state.User.createPassword;
});

const User = t.struct({
	name: t.String,
	email: t.String,
	mobile: t.Number,
	address: t.String,
	licenseNumber: t.Number,
	licenseExpiryDate: t.String,
	commencementDate: t.String,
	createPassword: Password,
	confirmPassword: confirmedPassword
})

var options = {
	fields: {
		name: {
			placeholder: 'John Doe',
			help: 'Please enter your full name'
		},
		createPassword: {
			help: 'Must be 8 characters or more'
		},
		licenseExpiryDate: {
			placeholder: 'dd/mm/yyyy'
		},
		commencementDate: {
			placeholder: 'dd/mm/yyyy'
		}
	}
}

export default class SignUpForm extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {};
	}

	onSubmit = (e) => {
		e.preventDefault();
		var value = this.refs.form.getValue();
		if (value) {
			console.log(value);
		}
	}

	onChange(value) {
		this.setState({ value });
		if (value.confirmPassword != null && value.confirmPassword !="") {

		}
	}

	clearForm() {
		this.setState({ value: null });
	}

	render() {
		return(
			<ScrollView>
				<View style={styles.container}>
					<Form
						ref="form"
						type={User}
						options={options}
						onChange={(value) => this.onChange(value)}
					/>

					<TouchableHighlight style={styles.button} onPress={this.onSubmit.bind(this)}>
						<Text style={styles.buttonText}>Sign Up</Text>
					</TouchableHighlight>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    margin: 0,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  buttonText: {
    fontSize: 18,
    color: '#ffffff',
    alignSelf: 'center'
  },
  button: {
    height: 48,
    width: 100,
    backgroundColor: '#FF7F00',
    borderColor: '#FF7F00',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

