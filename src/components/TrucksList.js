import React, { Component } from 'react';
import {
	Text,
	View,
	ScrollView
} from 'react-native';

import { List, ListItem } from 'react-native-elements';
import { trucks } from '../config/data.js';

export default class TrucksList extends Component {
	render() {
		return(
			<ScrollView>
				<List>
					{trucks.map((truck) => (
						<ListItem
							key={truck.modelNumber}
							title={truck.distnaceRun}
							subtitle={truck.maintainenceDue}
						/>
					))}
				</List>
			</ScrollView>
		);
	}
}