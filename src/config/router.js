import React from 'react';
import { 
	TabNavigator,
	StackNavigator,
	TabBarBottom
} from 'react-navigation';

import MainHomescreen from '../components/MainHomescreen.js';
import SignUpScreen from '../components/SignUpForm.js';
import TrucksList from '../components/TrucksList.js';
import WelcomeScreen from '../components/WelcomeScreen.js';

import ModalSignUpScreen from '../components/ModalSignUpScreen.js';

import { Icon } from 'react-native-elements';

/****************** Stacks ****************
*****************************************/

export const SignUpModalStack = StackNavigator({
	SigningUpModal: {
		screen: ModalSignUpScreen,
		mode: 'modal',
		navigationOptions: {
			title: 'Sign Up',
			headerStyle: {
				backgroundColor: '#FF7F00'
			},
			headerBorderColor: '#FF7F00',
			headerTintColor: '#ffffff',
			headerTitleStyle: {
				fontWeight: 'bold'
			}
		}
	}
})

export const WelcomeStack = StackNavigator({
	Welcome: {
		screen: WelcomeScreen,
		navigationOptions: {
			title: 'Welcome',
			headerStyle: {
				backgroundColor: '#FF7F00'
			}
		}
	},
	SignUp: {
		screen: SignUpScreen
	},
	mode: 'modal',
});

export const MainStack = StackNavigator({
	Main: {
		screen: MainHomescreen,
		navigationOptions: {
			title: 'Home',
			headerStyle: {
				backgroundColor: '#FF7F00'
			}
		}
	}
},{
	mode: 'modal',
	headerMode: 'none'
})

export const SignUpStack = StackNavigator({
	SignUp: {
		screen: SignUpScreen,
		navigationOptions: {
			title: 'SignUp',
			headerStyle: {
				backgroundColor: '#FF7F00'
			}
		}
	}
})

export const TrucksListStack = StackNavigator({
	Trucks: {
		screen: TrucksList,
		navigationOptions: {
			title: 'Trucks',
			headerStyle: {
				backgroundColor: '#FF7F00'
			}
		}
	}
})

/*************** Stacks-END **************
*****************************************/

export const Tabs = TabNavigator({
	Welcome: {
		screen: WelcomeStack,
		navigationOptions: {
			tabBarVisible: false
		}
	},
	SignUpModal: {
		screen: SignUpModalStack,
		navigationOptions: {
			tabBarVisible: false
		}
	},
	Main: {
		screen: MainStack,
		navigationOptions: {
			tabBarLabel: 'Home',
			tabBarIcon: ({ tintColor }) => (
				<Icon
					name={'list'}
					size={35}
					style={{color: tintColor}}
				/>
			)
		}
	},
	SignUp: {
		screen: SignUpStack,
		navigationOptions: {
			tabBarLabel: 'SignUp',
			tabBarIcon: ({ tintColor }) => (
				<Icon
					name={'account-circle'}
					size={35}
					style={{color: tintColor}}
				/>
			)
		}
	},
	TrucksList: {
		screen: TrucksListStack,
		navigationOptions: {
			tabBarLabel: 'Trucks',
			tabBarIcon: ({ tintColor }) => (
				<Icon
					name={'view-list'}
					size={35}
					style={{color: tintColor}}
				/>
			)
		}
	}
},
{
	lazy: true,
    tabBarComponent: props => {
      const backgroundColor = props.position.interpolate({
        inputRange: [0, 1, 2],
        outputRange: ['#FF7F00', '#FF7F00', '#FF7F00'],
      })
      return (
        <TabBarBottom
          {...props}
          style={{ backgroundColor: backgroundColor }}
        />
      );
    },
	tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'white',
    },
	tabBarPosition: 'bottom'
})